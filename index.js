teams = {
  powerRangers: [
    [44, 23, 71],
    [65, 54, 49],
  ],
  FairyTails: [
    [65, 54, 49],
    [23, 34, 47],
  ],
};
function calcAverage(array) {
  sum = 0;
  for (let i = 0; i < array.length; i++) {
    sum += array[i];
  }
  return sum / array.length;
}
function checkWinner(firstTeam, secondTeam) {
  firstTeam = calcAverage(firstTeam);
  secondTeam = calcAverage(secondTeam);
  if (firstTeam > secondTeam * 2) {
    return `${firstTeam} is winner `;
  } else if (secondTeam > firstTeam * 2) {
    return `${secondTeam} is winner`;
  } else {
    return "draw";
  }
}
console.log(checkWinner(teams.powerRangers[0], teams.FairyTails[0]));
console.log(checkWinner(teams.powerRangers[1], teams.FairyTails[1]));

// 2 task
let stats = {
  cash: [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52],
  tips: [],
  total: [],
};

function calcTip(cash) {
  let theEnd = 0;
  if (cash >= 50 && cash <= 300) {
    theEnd = cash * 0.15;
  } else {
    theEnd = cash * 0.2;
  }
  return theEnd;
}
console.log(calcTip(4));
function getTips(arr) {
  for (let i = 0; i < arr.length; i++) {
    stats.tips.push(calcTip(arr[i]));
  }
  return stats.tips;
}
console.log(getTips(stats.cash));

function sumMoney(totalMoney) {
  for (let i = 0; i < totalMoney.length; i++) {
    stats.total.push(totalMoney[i] + calcTip(totalMoney[i]));
  }
  return stats.total;
}
console.log(sumMoney(stats.cash));

function artAve(array) {
  let sum = 0;
  for (const par of array) {
    sum += par;
  }
  sum /= array.length;
  return sum;
}
console.log(artAve(stats.tips));
console.log(artAve(stats.total));
